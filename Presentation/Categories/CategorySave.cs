﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Services;
using Dal.FormModels;

namespace Presentation.Categories
{
    public partial class CategorySave : Form
    {
        private readonly CategoriesService _service;
        public CategorySave(Category category = null)
        {
            InitializeComponent();
            _service = new CategoriesService();

            FillForm(category);
        }

        public void FillForm(Category category)
        {
            if (category != null)
            {
                txtId.Text = category.Id.ToString();
                txtName.Text = category.Name;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                var id = string.IsNullOrWhiteSpace(txtId.Text)
                                    ? (int?)null
                                    : int.Parse(txtId.Text);

                var category = new Category(id, txtName.Text);

                _service.SaveCategory(category);
                MessageBox.Show("Išsaugota sėkmingai");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Nepavyko išsaugoti!{Environment.NewLine}{ex.Message}");
            }

        }
    }
}
