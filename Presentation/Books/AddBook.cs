﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FormModels = Dal.FormModels;
using Models = Dal.Models;
using Services;


namespace Presentation.Books
{
    public partial class AddBook : Form
    {
        private readonly BooksService _bookService;
        private readonly AuthorsService _authorsService;
        private readonly CategoriesService _categoriesService;
        public AddBook()
        {
            InitializeComponent();
            _bookService = new BooksService();
            _authorsService = new AuthorsService();
            _categoriesService = new CategoriesService();
            ConfigureForm();
        }

        ~AddBook()
        {
            _bookService.Dispose();
            _authorsService.Dispose();
            _categoriesService.Dispose();
        }

        public void ConfigureForm()
        {
            listBoxAuthors.DisplayMember = "Fullname";
            listBoxAuthors.ValueMember = "Id";

            listBoxCategories.DisplayMember = "Name";
            listBoxCategories.ValueMember = "Id";

            FillAuthorsListbox();
            FillCategoriesListbox();
        }

        public void FillAuthorsListbox()
        {
            var authors = _authorsService.GetAuthors();
            listBoxAuthors.DataSource = new BindingSource(authors, null);
            listBoxAuthors.SelectedItem = null;
        }

        public void FillCategoriesListbox()
        {
            var categories = _categoriesService.GetCategoriesNew();
            listBoxCategories.DataSource = new BindingSource(categories, null);
            listBoxCategories.SelectedItem = null;
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            var bookAdd = new FormModels.BookAdd
            {
                Title = txtTitle.Text,
                DateOfPublish = dateDateOfPublish.Value,
                CategoryIds = new List<int>(),
                AuthorIds = new List<int>()
            };

            foreach (Models.CategoryModel category in listBoxCategories.SelectedItems)
            {
                bookAdd.CategoryIds.Add(category.Id);
            }

            foreach (FormModels.Author author in listBoxAuthors.SelectedItems)
            {
                bookAdd.AuthorIds.Add(author.Id.Value);
            }

            _bookService.BookAdd(bookAdd);
        }
    }
}
