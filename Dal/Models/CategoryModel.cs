﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Models
{
    [Table("categories")]
    public class CategoryModel
    {
        [Key]
        public int Id { get; private set; }

        public string Name { get; private set; }

        public bool IsDeleted { get; private set; }

        private CategoryModel () { }

        public CategoryModel (string name, bool isDeleted = false)
        {
            Name = name;
            IsDeleted = isDeleted;
        }

        public void Update(string name)
        {
            Name = name;
        }
    }
}
