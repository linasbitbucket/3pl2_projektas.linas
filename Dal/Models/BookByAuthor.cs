﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Models
{
    [Table("booksbyauthor")]
    public class BookByAuthor
    {
        [Key]
        [Column(Order = 1)]
        public int BookId { get; private set; }

        [Key]
        [Column(Order = 2)]
        public int AuthorId { get; private set; }

        private BookByAuthor() { }

        public BookByAuthor(int bookId, int authorId)
        {
            BookId = bookId;
            AuthorId = authorId;
        }
    }
}
